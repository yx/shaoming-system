<?php
namespace Admin\Controller;
use Think\Controller;
class SystemController extends Controller {
    //系统登录页
    public function login(){
        if($_POST){
            //设置成功后跳转页面的地址，默认的返回页面是$_SERVER['HTTP_REFERER']
            $this->success('登录成功', '/Index/index');
        }
        $this->display();
    }

    //退出登录

}